namespace JWobot.Models
{
    public class JWobotMessageModel
    {
        public JWobotMessageModel()
        {
            Messages = new List<OpenAIMessageModel>();
        }
        public string Token { get; set; }

        public List<OpenAIMessageModel> Messages { get; set; }
    }
}