namespace JWobot.Models
{
    public class OpenAIMessageModel
    {
        public string role { get; set; }
        public string content { get; set; }
    }
}