﻿namespace JWobot.Models
{
    public class ChatBotModel
    {
        public string Name { get; set; }
        public string Company { get; set; }
        public string Token { get; set; }
        public string Data { get; set; }

    }
}
