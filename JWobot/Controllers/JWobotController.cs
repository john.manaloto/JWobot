﻿using JWobot.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using static System.Net.WebRequestMethods;
using System.Text;
using Newtonsoft.Json;
using JWobotBL;
using AutoMapper;
using Newtonsoft.Json.Linq;
using Azure;

namespace JWobot.Controllers
{
    public class JWobotController : Controller
    {
        private readonly IChatBotService _chatBotService;
        private readonly IUncertainPhraseService _uncertainPhraseService;
        private readonly IMapper _mapper;

        public JWobotController(ILogger<HomeController> logger, IChatBotService chatBotService, IUncertainPhraseService uncertainPhraseService, IMapper mapper)
        {
            _chatBotService = chatBotService;
            _uncertainPhraseService = uncertainPhraseService;
            _mapper = mapper;
        }

        public IActionResult GetByToken(string token)
        {
            var cb = _chatBotService.GetByToken(token);

            return Ok(_mapper.Map<ChatBotModel>(cb));
        }

        [HttpPost]
        public async Task<IActionResult> SendAsync([FromBody] JWobotMessageModel model)
        {
            var message = new OpenAIMessageModel();
            var cb = _chatBotService.GetByToken(model.Token);
            if (cb != null)
            {
                model.Messages.Insert(0, new OpenAIMessageModel
                {
                    role = "system",
                    content = "You are a chatbot for " + cb.Company + " Website. Your name is " + cb.Name + ". Answer all questions, inquiries or concerns based on the data provided. Alyways provide a concise answer." +
                    "Do not answer any questions that is not related to the data below. Say that you're only allowed to answer questions related to " + cb.Company + "." +
                    "\n\n**About " + cb.Company + ":** " + cb.Data
                    //"An integral part of the internet revolution for the last 24+ years, JWay conducted R&D with the National Centre for Supercomputing Applications at the University of Illinois, a pioneer of supercomputing, the modern Internet, World Wide Web, and Apache HTTP Server." +
                    //"We’ve worked with enterprise companies such as D.W. Morgan, Go2Group, Omnicell, Build It Green, IDT, Cisco and FMC, to name a few, on cutting-edge Internet, Web, Mobile, SFDC, Digital Marketing and Cloud and Big Data Projects." +
                    //"In 2011, we expanded our operations to include a Silicon-Valley-style Development Center in the Philippines and are in a unique position to offer our customers excellent quality and digital solutions at competitive pricing." +
                    //"The services of JWay are Technology, Digital Marketing, Content Marketing and HR Recruiting." +
                    //"The Technology service composed of Web Design and App Development, Mobile Apps, UI/UX Design, Logistics, Technology Consulting, Cybersecurity, Technical Support and CRM." +
                    //"The Digital Marketing service composed of Social Media." +
                    //"The Content Marketing service composed of Graphic Design." +
                    //"The HR Recruiting service composed of Recruitment." +
                    //"Here are some of the people behind JWay." +
                    //"Joe Choe - CEO" +
                    //"Ben Gomez - CFO" +
                    //"Rex Mupas - CTO" +
                    //"Kelly Hemingway - CMO" +
                    //"Ian Lopez - Chief of Staff"
                    //content = "Welcome to " + cb.Name + ", the chatbot for " + cb.Company + "'s website. Here's how I can assist you and some information about " + cb.Company + ": " +
                    //        "\n\n**Instructions:** " +
                    //        "\n- I specialize in answering questions related to " + cb.Company + " and its services. If you have any inquiries, concerns, or need information about " + cb.Company + ", feel free to ask, and I'll provide a concise answer." +
                    //        "\n- Please ensure that your questions are relevant to " + cb.Company + "'s areas of expertise to receive accurate responses." +
                    //        "\n- I am strictly prohibited to answer unrelated question to " + cb.Company + "." +
                    //        "\n\n**About " + cb.Company + ":** " +
                    //        cb.Data
                    //"\nAn integral part of the internet revolution for the last 24+ years, JWay conducted R&D with the National Centre for Supercomputing Applications at the University of Illinois, a pioneer of supercomputing, the modern Internet, World Wide Web, and Apache HTTP Server." +
                    //"\nWe’ve worked with enterprise companies such as D.W. Morgan, Go2Group, Omnicell, Build It Green, IDT, Cisco and FMC, to name a few, on cutting-edge Internet, Web, Mobile, SFDC, Digital Marketing and Cloud and Big Data Projects." +
                    //"\nIn 2011, we expanded our operations to include a Silicon-Valley-style Development Center in the Philippines and are in a unique position to offer our customers excellent quality and digital solutions at competitive pricing." +
                    //"\nThe services of JWay are Technology, Digital Marketing, Content Marketing, and HR Recruiting." +
                    //"\nThe Technology service composed of Web Design and App Development, Mobile Apps, UI/UX Design, Logistics, Technology Consulting, Cybersecurity, Technical Support, and CRM." +
                    //"\nThe Digital Marketing service composed of Social Media." +
                    //"\nThe Content Marketing service composed of Graphic Design." +
                    //"\nThe HR Recruiting service composed of Recruitment." +
                    //"\nHere are some of the people behind JWay:" +
                    //"\n- Joe Choi - CEO" +
                    //"\n- Ben Gomez - CFO" +
                    //"\n- Rex Mupas - CTO" +
                    //"\n- Kelly Hemingway - CMO" +
                    //"\n- Ian Lopez - Chief of Staff"
                });

                var http = new HttpClient();

                //var apiKey = "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";
                var apiKey = "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";
                http.DefaultRequestHeaders.Add("Authorization", $"Bearer {apiKey}");

                // JSON content for the API call
                var jsonContent = new
                {
                    messages = model.Messages,
                    model = "gpt-3.5-turbo",
                    max_tokens = 1000
                };

                try
                {

                    // Make the API call
                    var responseContent = await http.PostAsync("https://api.openai.com/v1/chat/completions", new StringContent(JsonConvert.SerializeObject(jsonContent), Encoding.UTF8, "application/json"));

                    // Read the response as a string
                    var resContext = await responseContent.Content.ReadAsStringAsync();

                    // Deserialize the response into a dynamic object
                    var data = JsonConvert.DeserializeObject<dynamic>(resContext);
					var responseMessage = data.choices[0].message.content.ToString();

                    message = new OpenAIMessageModel
                    {
                        role = "assistant",
                        content = _uncertainPhraseService.IsUncertain(responseMessage) ? cb.FallbackMessage : responseMessage
                    };
                }
                catch (Exception ex)
                {
                    message = new OpenAIMessageModel
                    {
                        role = "system",
                        content = "An error has occured, sorry for the inconvenience. Please try again later."
                    };
                }
            }
            else
            {
                return BadRequest("Invalid token");
            }
            return Ok(message);
        }
	}
}