﻿using AutoMapper;
using JWobot.Models;
using JWobotDAL;

namespace JWobot.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ChatBot, ChatBotModel>()
                .ReverseMap();
        }
    }
}
