﻿var jwobot = function () {
    const jwobotContainer = $('.jwobot-container');
    let wrapper,
        token,
        conversation,
        jwobotMessage,
        jwobotSend,
        iconClosed,
        iconOpen,
        messages = [];

    return {
        init: init
    }

    async function init(settings) {

        if (settings.apiDomain == undefined) {
            settings.apiDomain = 'https://localhost:7086';
        }
        jwobotContainer.attr('id', settings.token);
        token = settings.token;

        var cb = await getByToken();
        if (cb == undefined) {
            return false;
        }

        if (!jwobotContainer.hasClass('closed')) {
            jwobotContainer.addClass('closed');
        }

        if (jwobotContainer.hasClass('open')) {
            jwobotContainer.removeClass('open');
        }

        jwobotContainer.html(`
            <div class="wrapper" style="display:none;">
                <div class="header">
                    ${settings.logo != undefined ? `<img src="${settings.logo}"/>` : `<h3>${cb.name}</h3>`}
                    <a href="javascript:;" class="close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M0 0h24v24H0z" fill="none" />
                            <path d="M18 6L6 18M6 6l12 12" stroke="#000" stroke-width="2" stroke-linecap="round" />
                        </svg>
                    </a>
                </div>
                <div class="messages">
                </div>
                <div class="input-container">
                    <input id="jwobotMessage" type="text" placeholder="Type your message here..." />
                    <button id="jwobotSend">
                        <svg xmlns="http://www.w3.org/2000/svg"
                             width="34"
                             height="34"
                             viewBox="0 0 25 25"
                             fill="none"
                             stroke="white"
                             stroke-width="2"
                             stroke-linecap="round"
                             stroke-linejoin="round">
                            <line x1="22" y1="2" x2="11" y2="13" />
                            <polygon points="22 2 15 22 11 13 2 9 22 2" />
                        </svg>

                    </button>
                </div>
            </div>
            <div class="icon-container">
                <img src="${settings.openImg}" class="icon-open" />
                <img src="${settings.closedImg}" class="icon-closed" />
            </div>
        `);

        wrapper = jwobotContainer.find('.wrapper');
        conversation = jwobotContainer.find('.messages');
        jwobotMessage = $('#jwobotMessage');
        jwobotSend = $('#jwobotSend');
        iconClosed = jwobotContainer.find('.icon-closed');
        iconOpen = jwobotContainer.find('.icon-open');

        $(document).ready(function () {

            iconClosed.click(function () {
                if (conversation.children().length == 0) {
                    conversation.append(`<div class="response">Hello! My name is ${cb.name}. How can I assist you today?</div>`);
                }

                jwobotContainer.removeClass('closed');
                jwobotContainer.addClass('open');
                wrapper.fadeIn();
            });

            iconOpen.click(function () {
                wrapper.fadeOut(function () {
                    jwobotContainer.addClass('closed');
                    jwobotContainer.removeClass('open');
                });
            });

            jwobotContainer.find('.header .close').click(function () {
                iconOpen.trigger('click');
            })

            jwobotMessage.keydown(function (e) {
                if (e.which == 13) {
                    jwobotSend.trigger('click');
                }
            });

            jwobotSend.click(async function () {
                const msg = jwobotMessage.val();

                if (msg != '') {
                    appendMessage({
                        role: "user",
                        content: msg
                    });
                    jwobotMessage.val('');
                    const loader = $(`<div class="response typing-animation">
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                    </div>`).appendTo(conversation);
                    conversation.scrollTop(conversation[0].scrollHeight);
                    const result = await sendMessage();
                    if (result != null && result.role != null) {
                        appendMessage(result);
                    }
                    loader.remove();
                }
            })
        })

        function appendMessage(message) {
            let cls = 'question';
            if (message.role != 'user') {
                cls = 'response';
            }
            conversation.append('<div class="' + cls + '">' + message.content.replace(/\n/g, '<br>') + '</div>');
            conversation.scrollTop(conversation[0].scrollHeight);
            messages.push(message);
        }

        async function sendMessage() {
            return $.ajax({
                url: settings.apiDomain + '/jwobot/send',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                    messages: messages,
                    token: token
                }),
                error: function (xhr, status, exception) {
                    alert('An error has occured. Please contact your system administrator.');
                    console.log(xhr);
                    console.log(status);
                    console.log(exception);
                }
            });
        }

        async function getByToken() {
            return $.ajax({
                url: settings.apiDomain + '/jwobot/getbytoken?token=' + token,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                error: function (xhr, status, exception) {
                    alert('An error has occured. Please contact your system administrator.');
                    console.log(xhr);
                    console.log(status);
                    console.log(exception);
                }
            });
        }
    }
}();