﻿using JWobotDAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWobotBL
{
    public class UncertainPhraseService : IUncertainPhraseService
    {
        private readonly DataContext _context;

        public UncertainPhraseService(DataContext context)
        {
            _context = context;
        }

        public async Task<UncertainPhrase> Create(UncertainPhrase uncertainPhrase)
        {
            try
            {
                if (_context.UncertainPhrases.Any(x => x.Phrase == uncertainPhrase.Phrase))
                    throw new Exception("\"" + uncertainPhrase.Phrase + "\" already exists");

                uncertainPhrase.CreatedDate = DateTime.Now;

                _context.UncertainPhrases.Add(uncertainPhrase);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

            return uncertainPhrase;
        }

        public async Task Delete(int id)
        {
            var UncertainPhrase = _context.UncertainPhrases.Find(id);
            if (UncertainPhrase != null)
            {
                UncertainPhrase.IsDeleted = true;
                await _context.SaveChangesAsync();
            }
        }

        public IEnumerable<UncertainPhrase> GetAll()
        {
            return _context.UncertainPhrases.Where(x => !x.IsDeleted).AsNoTracking();
        }

        public UncertainPhrase GetById(int id)
        {
            return _context.UncertainPhrases.Where(x => x.Id == id)
                                 .AsNoTracking()
                                 .FirstOrDefault();
        }

        public bool IsUncertain(string response)
        {
            return _context.UncertainPhrases.Where(x => EF.Functions.Like(response, "%" + x.Phrase + "%")).Any();
        }

        public async Task Update(UncertainPhrase upParam)
        {
            if (upParam == null)
                throw new Exception("Uncertain Phrase not found");

            var uncertainPhrase = _context.UncertainPhrases
                            .Where(x => x.Id == upParam.Id)
                            .FirstOrDefault();

            if (_context.UncertainPhrases.Any(x => x.Phrase == upParam.Phrase && !x.IsDeleted && x.Id != upParam.Id))
                throw new Exception("\"" + upParam.Phrase + "\" already exists");

            uncertainPhrase.Phrase = upParam.Phrase;
            uncertainPhrase.ModifiedBy = upParam.ModifiedBy;
            uncertainPhrase.ModifiedDate = DateTime.Now;

            _context.UncertainPhrases.Update(uncertainPhrase);
            await _context.SaveChangesAsync();
        }
    }
}
