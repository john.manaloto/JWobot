﻿using JWobotDAL;
using Microsoft.EntityFrameworkCore;

namespace JWobotBL
{
    public class ChatBotService : IChatBotService
    {
        private readonly DataContext _context;

        public ChatBotService(DataContext context)
        {
            _context = context;
        }
        public async Task<ChatBot> Create(ChatBot chatBot)
        {
            if (_context.ChatBots.Any(x => x.Name == chatBot.Name && !x.IsDeleted))
                throw new Exception("Name \"" + chatBot.Name + "\" already exists");

            if (_context.ChatBots.Any(x => x.Token == chatBot.Token && !x.IsDeleted))
                throw new Exception("Token \"" + chatBot.Token + "\" already exists");

            var cb = new ChatBot
            {
                Name = chatBot.Name,
                Company = chatBot.Company,
                Token = chatBot.Token,
                FallbackMessage = chatBot.FallbackMessage,
                Data = chatBot.Data,
                IsActive = chatBot.IsActive,
                CreatedBy = chatBot.CreatedBy,
                CreatedDate = DateTime.Now
            };
            _context.ChatBots.Add(cb);
            await _context.SaveChangesAsync();
            return cb;
        }

        public async Task Delete(int id)
        {
            var cb = _context.ChatBots.Find(id);
            if (cb != null)
            {
                cb.IsDeleted = true;
                await _context.SaveChangesAsync();
            }
        }

        public IEnumerable<ChatBot> GetAll()
        {
            return _context.ChatBots.Where(x => !x.IsDeleted).AsNoTracking().OrderBy(x => x.Name);
        }

        public ChatBot GetById(int id)
        {
            return _context.ChatBots.Find(id);
        }

        public ChatBot GetByToken(string token)
        {
            return _context.ChatBots.Where(x => x.IsActive && x.Token == token).FirstOrDefault();
        }

        public IEnumerable<ChatBot> GetByUserId(int userId)
        {
            return _context.ChatBots.Where(x => !x.IsDeleted && x.UserChatBots.Where(u => u.UserId == userId).Any()).AsNoTracking().OrderBy(x => x.Name);
        }

        public async Task Update(ChatBot cbParam)
        {
            var cb = _context.ChatBots.Find(cbParam.Id);

            if (_context.ChatBots.Any(x => x.Name == cbParam.Name && !x.IsDeleted && x.Id != cbParam.Id))
                throw new Exception("Name \"" + cbParam.Name + "\" already exists");

            if (_context.ChatBots.Any(x => x.Token == cbParam.Token && !x.IsDeleted && x.Id != cbParam.Id))
                throw new Exception("Token \"" + cbParam.Token + "\" already exists");

            cb.Name = cbParam.Name;
            cb.Company = cbParam.Company;
            cb.Token = cbParam.Token;
            cb.FallbackMessage = cbParam.FallbackMessage;
            cb.Data = cbParam.Data;
            cb.IsActive = cbParam.IsActive;
            cb.ModifiedBy = cbParam.ModifiedBy;
            cb.ModifiedDate = DateTime.Now;
            _context.ChatBots.Update(cb);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateData(ChatBot cbParam)
        {
            var cb = _context.ChatBots.Find(cbParam.Id);

            cb.FallbackMessage = cbParam.FallbackMessage;
            cb.Data = cbParam.Data;
            cb.ModifiedBy = cbParam.ModifiedBy;
            cb.ModifiedDate = DateTime.Now;
            _context.ChatBots.Update(cb);
            await _context.SaveChangesAsync();
        }
    }
}