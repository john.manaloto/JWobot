﻿using JWobotDAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWobotBL
{
    public class UserService : IUserService
    {
        private readonly DataContext _context;

        public UserService(DataContext context)
        {
            _context = context;
        }

        public async Task<User> Create(User user)
        {
            try
            {
                if (_context.Users.Any(x => x.Email == user.Email))
                    throw new Exception("Email \"" + user.Email + "\" is already taken");

                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(user.Email, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
                user.CreatedDate = DateTime.Now;

                if (!user.IsAdmin)
                {
                    foreach (var ucb in user.UserChatBots)
                    {
                        ucb.CreatedBy = user.CreatedBy;
                        ucb.CreatedDate = user.CreatedDate;
                    }
                }
                else
                {
                    user.UserChatBots.Clear();
                }

                _context.Users.Add(user);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

            return user;
        }

        public async Task Delete(int id)
        {
            var user = _context.Users.Find(id);
            if (user != null)
            {
                user.IsDeleted = true;
                await _context.SaveChangesAsync();
            }
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users.Where(x => !x.IsDeleted).AsNoTracking();
        }

        public User GetById(int id)
        {
            return _context.Users.Where(x => x.Id == id)
                                .Include(x => x.UserChatBots)
                                 .AsNoTracking()
                                 .FirstOrDefault();
        }

        public async Task Update(User userParam)
        {
            if (userParam == null)
                throw new Exception("User not found");

            var user = _context.Users
                            .Where(x => x.Id == userParam.Id)
                            .Include(x => x.UserChatBots)
                            .FirstOrDefault();

            // update username if it has changed
            if (!string.IsNullOrWhiteSpace(userParam.Email) && user.Email != userParam.Email)
            {
                // throw error if the new username is already taken
                if (_context.Users.Any(x => x.Email == userParam.Email && x.Id != userParam.Id))
                    throw new Exception("Email " + userParam.Email + " is already taken");
            }

            user.FirstName = userParam.FirstName;
            user.LastName = userParam.LastName;
            user.MiddleName = userParam.MiddleName;
            user.Email = userParam.Email;
            user.IsAdmin = userParam.IsAdmin;
            user.IsActive = userParam.IsActive;
            user.ModifiedBy = userParam.ModifiedBy;
            user.ModifiedDate = DateTime.Now;

            if (user.IsAdmin)
            {
                user.UserChatBots.Clear();
            }
            else
            {
                var existingChatBotIds = user.UserChatBots.Select(ucb => ucb.ChatBotId).ToArray();
                var seletedChatBotIds = userParam.UserChatBots.Select(x => x.ChatBotId);
                _context.UserChatBots.RemoveRange(user.UserChatBots.Where(ucb => !seletedChatBotIds.Contains(ucb.ChatBotId)));
                _context.UserChatBots.AddRange(seletedChatBotIds.Where(id => !existingChatBotIds.Contains(id)).Select(id => new UserChatBot
                {
                    UserId = user.Id,
                    ChatBotId = id,
                    CreatedBy = user.CreatedBy,
                    CreatedDate = user.CreatedDate
                }));
            }

            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public User Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _context.Users.SingleOrDefault(x => !x.IsDeleted && x.IsActive && x.Email == username);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            // authentication successful
            return user;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
    }
}
