﻿using JWobotDAL;

namespace JWobotBL
{
    public interface IUncertainPhraseService
    {
        IEnumerable<UncertainPhrase> GetAll();
        UncertainPhrase GetById(int id);
        Task<UncertainPhrase> Create(UncertainPhrase uncertainPhrase);
        Task Delete(int id);
        Task Update(UncertainPhrase upParam);

        bool IsUncertain(string response);
    }
}
