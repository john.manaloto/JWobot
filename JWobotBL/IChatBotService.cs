﻿using JWobotDAL;

namespace JWobotBL
{
    public interface IChatBotService
    {
        Task<ChatBot> Create(ChatBot chatBot);
        IEnumerable<ChatBot> GetAll();
        IEnumerable<ChatBot> GetByUserId(int userId);
        ChatBot GetById(int id);
        ChatBot GetByToken(string token);
        Task Update(ChatBot cbParam);
        Task UpdateData(ChatBot cbParam);
        Task Delete(int id);
    }
}
