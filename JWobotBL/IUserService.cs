﻿using JWobotDAL;

namespace JWobotBL
{
    public interface IUserService
    {
        IEnumerable<User> GetAll();
        User GetById(int id);
        Task<User> Create(User user);
        Task Delete(int id);
        Task Update(User userParam);
        User Authenticate(string username, string password);
    }
}
