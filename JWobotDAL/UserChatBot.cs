﻿using System.ComponentModel.DataAnnotations;

namespace JWobotDAL
{
    public class UserChatBot : BaseEntity
    {
        public int UserId { get; set; }
        public int ChatBotId { get; set; }

        public virtual User User { get; set; }
        public virtual ChatBot ChatBot { get; set; }

    }
}