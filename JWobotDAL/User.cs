﻿using System.ComponentModel.DataAnnotations;

namespace JWobotDAL
{
    public class User : BaseEntity
    {
        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string? MiddleName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        public byte[] PasswordHash { get; set; }

        [Required]
        public byte[] PasswordSalt { get; set; }
        [Required]
        public bool IsAdmin { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<UserChatBot> UserChatBots { get; set; }
    }
}