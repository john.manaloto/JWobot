﻿using System.ComponentModel.DataAnnotations;

namespace JWobotDAL
{
    public class ChatBot : BaseEntity
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Company { get; set; }

        [Required]
        [StringLength(50)]
        public string Token { get; set; }

        [Required]
        public string Data { get; set; }

		[Required]
		public string FallbackMessage { get; set; }
		public bool IsActive { get; set; }

        public virtual ICollection<UserChatBot> UserChatBots { get; set; }
    }
}