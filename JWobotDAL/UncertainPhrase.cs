﻿using System.ComponentModel.DataAnnotations;

namespace JWobotDAL
{
    public class UncertainPhrase : BaseEntity
    {
        [Required]
        public string Phrase { get; set; }
    }
}