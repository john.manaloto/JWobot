﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace JWobotDAL
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<ChatBot> ChatBots { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserChatBot> UserChatBots { get; set; }
        public DbSet<UncertainPhrase> UncertainPhrases { get; set; }
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/../JWobotAdmin/appsettings.json").Build();
            var builder = new DbContextOptionsBuilder<DataContext>();
            var connectionString = configuration.GetConnectionString("JWobotConnStr");
            builder.UseSqlServer(connectionString);
            return new DataContext(builder.Options);
        }
    }
}
