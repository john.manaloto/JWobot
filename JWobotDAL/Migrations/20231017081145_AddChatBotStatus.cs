﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace JWobotDAL.Migrations
{
    /// <inheritdoc />
    public partial class AddChatBotStatus : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "ChatBots",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "ChatBots");
        }
    }
}
