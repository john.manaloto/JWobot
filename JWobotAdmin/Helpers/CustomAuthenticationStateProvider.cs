﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System;
using Blazored.LocalStorage;
using JWobotDAL;

namespace JWobotAdmin.Helpers
{
    public class CustomAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly ILocalStorageService _localStorage;
        private readonly AppSettings _appSettings;

        public CustomAuthenticationStateProvider(ILocalStorageService localStorage, IOptions<AppSettings> appSettings)
        {
            _localStorage = localStorage;
            _appSettings = appSettings.Value;
        }
        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var token = await _localStorage.GetItemAsync<string>("token");

            ClaimsIdentity identity;

            if (!string.IsNullOrEmpty(token))
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                SecurityToken validatedToken;
                try
                {
                    var validatedClaims = tokenHandler.ValidateToken(token, new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    }, out validatedToken);

                    if (validatedToken.ValidTo > DateTime.Now)
                    {
                        var claims = new List<Claim>();
                        validatedClaims.Claims.Where(x => x.Type == ClaimTypes.Role || x.Type == ClaimTypes.Email || x.Type == ClaimTypes.Name || x.Type == "FirstName" || x.Type == "FullName").ToList().ForEach(x => claims.Add(x));
                        identity = new ClaimsIdentity(claims, "apiauth_type");
                    }
                    else
                    {
                        await _localStorage.RemoveItemAsync("token");
                        identity = new ClaimsIdentity();
                    }
                }
                catch (Exception ex)
                {
                    await _localStorage.RemoveItemAsync("token");
                    identity = new ClaimsIdentity();
                }

            }
            else
            {
                identity = new ClaimsIdentity();
            }

            var user = new ClaimsPrincipal(identity);
            return await Task.FromResult(new AuthenticationState(user));
        }

        public async void MarkUserAsAuthenticated(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim("FirstName", user.FirstName),
                new Claim("FullName", $"{user.FirstName} {user.LastName}"),
            };


            claims.Add(new Claim(ClaimTypes.Role, user.IsAdmin ? "Admin" : "User"));

            var identity = new ClaimsIdentity(claims, "apiauth_type");
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identity,
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            await _localStorage.SetItemAsync("token", tokenString);

            var userClaims = new ClaimsPrincipal(identity);
            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(userClaims)));
        }

        public void MarkUserAsLoggedOut()
        {
            _localStorage.RemoveItemAsync("token");
            var identity = new ClaimsIdentity();
            var user = new ClaimsPrincipal(identity);
            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(user)));
        }

        public string GetCurrentUserEmail(ClaimsPrincipal claimsPrincipal)
        {
            var email = "";
            if (claimsPrincipal != null)
            {
                email = claimsPrincipal.Claims.Where(c => c.Type == ClaimTypes.Email).Select(c => c.Value).SingleOrDefault();
                if (string.IsNullOrEmpty(email))
                {
                    email = "System";
                }
            }
            return email;
        }
    }
}
