﻿using Microsoft.JSInterop;

namespace JWobotAdmin.Helpers
{
    public class JSInterop
    {
        private readonly IJSRuntime _js;
        public JSInterop(IJSRuntime js)
        {
            _js = js;
        }

        public async Task ChangeBodyClass(string className)
        {
            await _js.InvokeVoidAsync("changeBodyClass", className);
        }

        public async Task ShowLoader()
        {
            await _js.InvokeVoidAsync("showLoader");
        }

        public async Task HideLoader()
        {
            await _js.InvokeVoidAsync("hideLoader");
        }

        public async Task ShowError(string title, string msg)
        {
            await _js.InvokeVoidAsync("showError", msg, title);
        }

        public async Task ShowSuccess(string title, string msg)
        {
            await _js.InvokeVoidAsync("showSuccess", msg, title);
        }

        public async Task InitSidebarToggle()
        {
            await _js.InvokeVoidAsync("initSidebarToggle");
        }

        public async Task SampleAreaChart()
        {
            await _js.InvokeVoidAsync("sampleAreaChart");
        }
    }
}
