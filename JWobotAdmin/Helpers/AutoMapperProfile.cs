﻿using AutoMapper;
using JWobotAdmin.Models;
using JWobotDAL;

namespace JWobotAdmin.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ChatBot, ChatBotModel>()
                .ReverseMap();

            CreateMap<User, UserModel>()
                .ForMember(dest => dest.ChatBotIds, opt => opt.MapFrom(src => src.UserChatBots.Select(ucb => (int?)ucb.ChatBotId).ToArray()))
                .ReverseMap()
                .ForMember(dest => dest.UserChatBots, opt => opt.MapFrom(src => src.ChatBotIds.Select(id => new UserChatBot { ChatBotId = id.Value })));

            CreateMap<User, UserListModel>()
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.IsAdmin ? "Admin" : "Client"))
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.IsActive ? "Yes" : "No"));

            CreateMap<ChatBot, ChatBotListModel>()
                .ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.IsActive ? "Yes" : "No"));

            CreateMap<UncertainPhrase, UncertainPhraseModel>()
                .ReverseMap();
        }
    }
}
