﻿using System.ComponentModel.DataAnnotations;

namespace JWobotAdmin.Models
{
    public class ChatBotModel
    {
        public ChatBotModel()
        {
            IsActive = true;
        }
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Company is required")]
        [StringLength(100)]
        public string Company { get; set; }

        [Required(ErrorMessage = "Token is required")]
        [StringLength(50)]
        public string Token { get; set; }

        [Required(ErrorMessage = "Fallback Message is required")]
        public string FallbackMessage { get; set; }

        [Required(ErrorMessage = "Data is required")]
        public string Data { get; set; }
        public bool IsActive { get; set; }
    }
}
