﻿using System.ComponentModel.DataAnnotations;

namespace JWobotAdmin.Models
{
    public class UserListModel
    {
        public UserListModel()
        {
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Active { get; set; }
    }
}
