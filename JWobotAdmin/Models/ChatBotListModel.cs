﻿using System.ComponentModel.DataAnnotations;

namespace JWobotAdmin.Models
{
    public class ChatBotListModel
    {
        public ChatBotListModel()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Token { get; set; }
        public string Active { get; set; }
    }
}
