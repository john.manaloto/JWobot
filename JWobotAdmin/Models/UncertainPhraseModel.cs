﻿using System.ComponentModel.DataAnnotations;

namespace JWobotAdmin.Models
{
    public class UncertainPhraseModel
    {
        [Required(ErrorMessage = "Phrase is required")]
        public string Phrase { get; set; }
    }
}
