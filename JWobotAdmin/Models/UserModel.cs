﻿using System.ComponentModel.DataAnnotations;

namespace JWobotAdmin.Models
{
    public class UserModel
    {
        public UserModel()
        {
            IsActive = true;
        }
        [Required(ErrorMessage = "First name is required")]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(100)]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Email address is required")]
        [StringLength(100)]
        [EmailAddress(ErrorMessage = "The Email address is not a valid e-mail address")]
        public string Email { get; set; }
        public int?[] ChatBotIds { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsActive { get; set; }
    }
}
